package com.POLHash.manager.controller;

import java.io.File;
import android.app.Activity;
import android.app.DialogFragment;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.AbsListView.MultiChoiceModeListener;

import com.POLHash.manager.Browser;
import com.POLHash.manager.R;
import com.POLHash.manager.dialogs.FileMultifilesDialog;

public final class ActionModeController {

	private final MultiChoiceModeListener multiChoiceListener;

	private final Activity mActivity;

	private AbsListView mListView;

	private ActionMode mActionMode;

	public ActionModeController(final Activity activity) {
		this.mActivity = activity;
		this.multiChoiceListener = new MultiChoiceListener();
	}

	public void finishActionMode() {
		if (this.mActionMode != null) {
			this.mActionMode.finish();
		}
	}

	public void setListView(AbsListView list) {
		if (this.mActionMode != null) {
			this.mActionMode.finish();
		}
		this.mListView = list;
		this.mListView.setMultiChoiceModeListener(this.multiChoiceListener);
	}

	private final class MultiChoiceListener implements MultiChoiceModeListener {
		final String mSelected = mActivity.getString(R.string._selected);

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			menu.clear();
			mActivity.getMenuInflater().inflate(R.menu.actionmode, menu);
			final int checkedCount = mListView.getCheckedItemCount();

			if (checkedCount > 1) {
			;
			}

			return true;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			ActionModeController.this.mActionMode = null;
		}

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			ActionModeController.this.mActionMode = mode;
			

			return true;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			final SparseBooleanArray items = mListView
					.getCheckedItemPositions();
			final int checkedItemSize = items.size();
			final String[] files = new String[mListView.getCheckedItemCount()];
			int index = -1;
			
			switch (item.getItemId()) {
			case R.id.actionmove: 

				//
				Log.v(null,""+ mListView.getCheckedItemCount());
				
				int key = 0;
				for (int i = 0; i < checkedItemSize; i++) {
					key = items.keyAt(i);
					if (items.get(key)) {
						files[++index] = (String) mListView
								.getItemAtPosition(key);
					}
				}

				final DialogFragment dialog = FileMultifilesDialog.instantiate(new File((String) mListView
						.getItemAtPosition(key)), files);
				mode.finish();
				dialog.show(mActivity.getFragmentManager(), Browser.TAG_DIALOG);
				return true;					

			default:
				return false;
			}
		}

		@Override
		public void onItemCheckedStateChanged(ActionMode mode, int position,
				long id, boolean checked) {
			mode.setTitle(mListView.getCheckedItemCount() + mSelected);
			mode.invalidate();
		}
	}
}
