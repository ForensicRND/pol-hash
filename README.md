## POL-HASH ##

### 본 프로젝트는 free open-source project Simple Explorer를 사용했습니다. ###

* POL-HASH is **Free software** and its source code is released under GNU General Public License version 3.0 (GPLv3).

* 경찰청 사이버안전국 디지털포렌식센터 포렌식연구개발팀 FRND@police.go.kr

This application use Open Source Software (OSS). You can find the source code of these open source projects, along with applicable license information, below. We are deeply grateful to these developers for their work and contributions.
### Original Project is Simple Explorer (GPLv3) ###
* Original Project - Simple Explorer, Copyright 2013 - 2015 Daniel F.

* Original Project- https://github.com/DF1E/SimpleExplorer

* More info under: https://www.gnu.org/licenses/gpl-3.0

### Using Project is iTextPDF ###
* http://itextpdf.com

* https://github.com/itext/itextpdf

* iText is licensed as AGPL software. AGPL is a free / open source software license.