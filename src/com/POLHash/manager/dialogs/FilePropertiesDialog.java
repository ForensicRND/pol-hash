package com.POLHash.manager.dialogs;

import java.io.File;
import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jetbrains.annotations.NotNull;

import com.POLHash.manager.R;
import com.POLHash.manager.utils.MD5Checksum;
import com.POLHash.manager.utils.SimpleUtils;

import org.apache.commons.io.FileUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

public final class FilePropertiesDialog extends DialogFragment {

	private static Activity activity;
	private static File mFile;
	private PropertiesAdapter mAdapter;
	public static String g_MD5;
	public static String g_SHA1;
	public static String g_size;
	public static String g_createtime;
	public static String g_device;
	public static String g_IMEI;
	public static String telPhoneNo;	
	public static String g_Singlereport;
	public static String g_Singlereport_time;
	
	private static Handler mHandler = new Handler();
	private static ProgressDialog pDlg;
	
	public static DialogFragment instantiate(File file) {
		mFile = file;
	
		final FilePropertiesDialog dialog = new FilePropertiesDialog();
		return dialog;
	}

	@SuppressLint("InflateParams") @Override
	public Dialog onCreateDialog(Bundle state) {
		activity = getActivity();
		final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		mAdapter = new PropertiesAdapter(activity, mFile);
		
		TelephonyManager telephony =  (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);  
	    telPhoneNo = telephony.getLine1Number();  			  
	    g_IMEI = telephony.getDeviceId();
	    
		builder.setTitle("파일 해시값 생성 정보");
		builder.setNegativeButton(R.string.sms,
			new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
					;
			}
		});	
		
		builder.setNeutralButton(R.string.report,
				new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) 
				{
					;
				}
			});		

		final View content = activity.getLayoutInflater().inflate( R.layout.dialog_properties_container, null);
		
		this.initView(content);
		
		builder.setView(content);
		
		final AlertDialog dialog = builder.create();
		dialog.setOnShowListener(new DialogInterface.OnShowListener() {
			@Override
			public void onShow(final DialogInterface dialog) {
				((AlertDialog) dialog).getButton(
						DialogInterface.BUTTON_POSITIVE).setVisibility(
						View.GONE);
			}
		});
		return dialog;
	}

	private void initView(@NotNull final View view) {
		final ViewPager pager = (ViewPager) view
				.findViewById(R.id.tabsContainer);
		pager.setAdapter(mAdapter);

		final CompoundButton tab1 = (CompoundButton) view
				.findViewById(R.id.tab1);
		final CompoundButton tab2 = (CompoundButton) view
				.findViewById(R.id.tab2);

		pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				tab1.setChecked(position == 0);
				tab2.setChecked(position == 1);
				((AlertDialog) getDialog())
						.getButton(DialogInterface.BUTTON_POSITIVE)
						.setVisibility(
								position == 0
										|| !((FilePermissionsPagerItem) mAdapter
												.getItem(1)).areBoxesEnabled() ? View.GONE
										: View.VISIBLE);
			}
		});

		tab1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tab1.setChecked(true);
				tab2.setChecked(false);
				pager.setCurrentItem(0);
			}
		});

		tab2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tab2.setChecked(true);
				tab1.setChecked(false);
				pager.setCurrentItem(1);
			}
		});
	}

	@Override
	public void onStart() {
		super.onStart();
		mAdapter.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();
		mAdapter.onStop();
	}

	private interface PagerItem {

		void onStart();

		void onStop();

		@NotNull
		View onCreateView(@NotNull LayoutInflater inflater);
	}

	private static final class PropertiesAdapter extends PagerAdapter {

		private final LayoutInflater mLayoutInflater;
		private final File mFile;
		private final PagerItem[] mItems;

		private PropertiesAdapter(@NotNull final Activity context,
				@NotNull final File file) {
			mLayoutInflater = context.getLayoutInflater();
			mFile = file;
			mItems = new PagerItem[] { new FilePropertiesPagerItem(mFile),
					new FilePermissionsPagerItem(mFile) };
		}

		void onStart() {
			for (final PagerItem item : mItems) {
				item.onStart();
			}
		}

		void onStop() {
			for (final PagerItem item : mItems) {
				item.onStop();
			}
		}

		@NotNull
		PagerItem getItem(final int position) {
			return mItems[position];
		}

		@Override
		public Object instantiateItem(final ViewGroup container,
				final int position) {
			final PagerItem item = mItems[position];
			final View view = item.onCreateView(mLayoutInflater);
			container.addView(view);
			item.onStart();
			return view;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public int getCount() {
			return mItems.length;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}
	}

	private static final class FilePropertiesPagerItem implements PagerItem {
		private File file3;
		private TextView mTimeLabel, mSizeLabel, mMD5Label, mSHA1Label, mNameLabel, mDeviceLabel;
		private View mView;
		private LoadFsTask mTask;

		private FilePropertiesPagerItem(File file) {
			this.file3 = file;
		}

		@SuppressLint("InflateParams") @NotNull
		@Override
		public View onCreateView(@NotNull final LayoutInflater inflater) {
			mView = inflater.inflate(R.layout.dialog_properties, null);
			initView(mView);
			return mView;
		}

		@Override
		public void onStart() {
			if (mView != null) {
				if (mTask == null) {
					mTask = new LoadFsTask();
					mTask.execute(mFile);
				}
			}
		}

		@Override
		public void onStop() {
			if (mTask != null && mTask.getStatus() == AsyncTask.Status.RUNNING) {
				mTask.cancel(true);
			}
		}

		private void initView(View table) {
			this.mNameLabel = (TextView) mView.findViewById(R.id.name_label);
			//this.mPathLabel = (TextView) mView.findViewById(R.id.path_label);
			this.mTimeLabel = (TextView) mView.findViewById(R.id.time_stamp);
			this.mSizeLabel = (TextView) mView.findViewById(R.id.total_size);
			this.mMD5Label = (TextView) mView.findViewById(R.id.md5_summary);
			this.mSHA1Label = (TextView) mView.findViewById(R.id.sha1_summary);
			this.mDeviceLabel = (TextView) mView.findViewById(R.id.device_summary);
		}

		private final class LoadFsTask extends AsyncTask<File, Void, String> {
			private String mDisplaySize;
			private long size = 0;

			@Override
			protected void onPreExecute() {
				mNameLabel.setText(mFile.getName());
				//mPathLabel.setText(file3.getAbsolutePath());
				mTimeLabel.setText("계산 중...");
				mSizeLabel.setText("계산 중...");
				mMD5Label.setText("계산 중...");
				mSHA1Label.setText("계산 중...");
				
				// activity.getFilesDir().getAbsolutePath()
//				mDeviceLabel.setText(activity.getFilesDir().getAbsolutePath());
				
				mDeviceLabel.setText(Build.MODEL + " (IMEI:" + g_IMEI + ")");
			}

			@Override
			protected String doInBackground(final File... params) {

				if (!file3.canRead()) {
					mDisplaySize = "---";
					return mDisplaySize;
				} else if (file3.isFile()) {
					size = file3.length();
				} else {
					size = SimpleUtils.getDirSize(file3);
				}
				
				mDisplaySize = FileUtils.byteCountToDisplaySize(size);
				DecimalFormat format = new DecimalFormat("###,###");
				String tmp = format.format(size);				

				g_size = tmp + " 바이트 (" + mDisplaySize +")";
				return tmp + " 바이트 (" + mDisplaySize +")";
			}
			
			public void setResutText() {
				mHandler.post(new Runnable() {
							
							@Override
							public void run() {
								mMD5Label.setText(g_MD5);
								mSHA1Label.setText(g_SHA1);	
							}
						});
			}

			@SuppressLint({ "DefaultLocale", "SimpleDateFormat" }) @Override
			protected void onPostExecute(final String result) {
				mSizeLabel.setText(result);
	
				long now = System.currentTimeMillis();
				Date date = new Date(now);
				SimpleDateFormat sdfNow = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				SimpleDateFormat sdfNow2 = new SimpleDateFormat("yyyyMMddHHmmss");
				g_Singlereport_time = sdfNow2.format(date);
				String strNow = sdfNow.format(date);
				DateFormat.getDateTimeInstance();
				// mTimeLabel.setText(df.format(file3.lastModified()));
				g_createtime = strNow;
				mTimeLabel.setText(strNow);				
				
				if (file3.isFile()) {
					try {
						
						// Progress 시작 // 방제완
						pDlg = new ProgressDialog(activity);
						pDlg.setMessage("대상 파일의 해시값을 계산 중 입니다...");
						pDlg.setIndeterminate(true);
						pDlg.setCancelable(false);
						pDlg.show();	
						
						new Thread(new Runnable() {
							
							@Override
							public void run() {
								try {									
									g_MD5 = MD5Checksum.getMD5Checksum(file3.getPath()).toUpperCase();
									g_SHA1 = MD5Checksum.getSHA1Checksum(file3.getPath()).toUpperCase();		
									pDlg.cancel();
									setResutText();
								} catch (Exception e) {
									e.printStackTrace();
								}
														
							}
						}).start();
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					// 폴더라면 나중에 그 안의 모든 파일에 대한 해시값 연산 추가
					mTimeLabel.setText("폴더의 해시값 생성을 지원하지 않습니다.");
					mMD5Label.setText("폴더의 해시값 생성을 지원하지 않습니다.");
					mSHA1Label.setText("폴더의 해시값 생성을 지원하지 않습니다.");
				
				}
			}
		}

	}

	@SuppressLint("InflateParams") private static final class FilePermissionsPagerItem implements PagerItem,
			CompoundButton.OnCheckedChangeListener {

		private final File mFile;
		private View mView;
		private LoadFsTask mTask;
		private TextView m_filename,m_MTime,m_ATime;
		private FilePermissionsPagerItem(final File file) {
			mFile = file;
		}

		@NotNull
		@Override
		public View onCreateView(@NotNull final LayoutInflater inflater) {
			mView = inflater.inflate(R.layout.dialog_permissions, null);
			initView(mView);
			return mView;
		}

		@Override
		public void onStart() {
			if (mView != null) {
				if (mTask == null) {
					mTask = new LoadFsTask(this);
				}
				if (mTask.getStatus() != AsyncTask.Status.RUNNING) {
					mTask.execute(mFile);
				}
			}
		}

		@Override
		public void onStop() {
			if (mTask != null && mTask.getStatus() == AsyncTask.Status.RUNNING) {
				mTask.cancel(false);
			}
		}
			
		@SuppressLint("SimpleDateFormat") private void initView(View table) {

			this.m_filename = (TextView) mView.findViewById(R.id.detail_name_label);
			//this.m_CurrentTime = (TextView) mView.findViewById(R.id.current_time);
			//this.m_CTime = (TextView) mView.findViewById(R.id.c_label);
			this.m_MTime = (TextView) mView.findViewById(R.id.m_label);
			this.m_ATime = (TextView) mView.findViewById(R.id.a_label);

			SimpleDateFormat sdfNow = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			String strNow;
			
			strNow = sdfNow.format(mFile.lastModified());
			m_filename.setText(mFile.getName());
			m_MTime.setText(strNow); // 수정 시각
			m_ATime.setText(mFile.getPath()); // 수정 시각
			// 현재 시간 출력
			/*
			Thread t = new Thread(new Runnable() 
			{
				
				public void run()
				{

					while(true) 
					{
						long now = System.currentTimeMillis();
						Date date = new Date(now);
						SimpleDateFormat sdfNow = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
						strtemp = sdfNow.format(date);
						m_CurrentTime.setText(sdfNow.format(date));	
						handler.post(new Runnable()
						{
							public void run()
							{
								m_CurrentTime.setText(strtemp);	
							}
						});
						
						try{
							Thread.sleep(1000);
						}catch(Exception e) {
							e.printStackTrace();
						}
					}
				}
			});
			t.start();*/
		}

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {/*
			this.mModifiedPermissions = new Permissions(this.ur.isChecked(),
					this.uw.isChecked(), this.ux.isChecked(),
					this.gr.isChecked(), this.gw.isChecked(),
					this.gx.isChecked(), this.or.isChecked(),
					this.ow.isChecked(), this.ox.isChecked());*/
		}

		private class LoadFsTask extends AsyncTask<File, Void, String> {

			private final WeakReference<FilePermissionsPagerItem> mItemRef;

			private LoadFsTask(@NotNull final FilePermissionsPagerItem item) {
				this.mItemRef = new WeakReference<FilePermissionsPagerItem>(
						item);
			}

			@Override
			protected String doInBackground(final File... params) {
				return params[0].getAbsolutePath();
			}

			@Override
			protected void onPostExecute(final String file) {
				final FilePermissionsPagerItem item = mItemRef.get();
				if (item != null) {
					if (file == null) {
						item.disableBoxes();
					}
				}
			}
		}

		boolean areBoxesEnabled() {
			return false;/*return this.ur.isEnabled();*/
		}

		private void disableBoxes() {

		}
	}
}
