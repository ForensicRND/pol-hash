package com.POLHash.manager;

import com.POLHash.manager.R;
import com.POLHash.manager.settings.Settings;
import com.stericson.RootTools.RootTools;

import android.app.Application;
import android.os.Environment;
import android.os.StrictMode;
import android.widget.Toast;

public final class SimpleExplorer extends Application {

	public static final int THEME_ID_LIGHT = 1;
	public static final int THEME_ID_DARK = 2;

	public static final String BACKUP_LOC = Environment
			.getExternalStorageDirectory().getPath() + "/Hash/Apps/";

	public static boolean rootAccess;
	public static String busybox;

	@Override
	public void onCreate() {
  	    StrictMode.enableDefaults();    	
		super.onCreate();
		
		// get default preferences
	    Settings.updatePreferences(this);
		checkEnvironment();
		
		rootAccess = RootTools.isAccessGiven();
	}

	// check for external storage exists
	private void checkEnvironment() {
		boolean sdCardExist = Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED);

		if (!sdCardExist) {
			Toast.makeText(this, getString(R.string.sdcardnotfound),
					Toast.LENGTH_SHORT).show();
		}
	}
}
