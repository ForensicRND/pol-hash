package com.POLHash.manager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import com.POLHash.manager.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.text.Editable;
import android.text.Selection;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("HandlerLeak") public class IntroActivity extends Activity {
	
	final private static int DIALOG_LOGIN = 1;
	EditText g_Name;
	EditText g_PhoneNumber;
	EditText g_Email;
	
	String strName;
	String strLocal;
	String strEmail;
	private Spinner spinner1;

	private String[] aa = {"지역을 선택하세요", "서울","부산","대구","인천","광주","대전","울산","경기","강원","충북","충남","전북","전남","경북","경남","제주"};
	//                        0            1      2      3      4      5      6      7      8      9      10     11     12     13     14     15     16

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); 
        setContentView(R.layout.intro_activity);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        
         Handler handler = new Handler () {
         @SuppressWarnings("deprecation")
		@Override
         public void handleMessage(Message msg) {
        	 showDialog(DIALOG_LOGIN);
        	 
         }
        };

        handler.sendEmptyMessageDelayed(0, 1777);

    }

    
    @SuppressLint("InflateParams") @Override
    protected Dialog onCreateDialog(int id) {
     AlertDialog dialogDetails = null;
    
     switch (id) {
     case DIALOG_LOGIN:
      LayoutInflater inflater = LayoutInflater.from(this);
      View dialogview = inflater.inflate(R.layout.user_dialog, null);  

      GetUserInfo();
      
      AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
      dialogbuilder.setView(dialogview);
      
       TextView title = new TextView(this);
	   title.setText("보고서 생성 정보 입력");
	   title.setBackgroundColor(Color.DKGRAY);
	   title.setPadding(10, 10, 10, 10);
	   title.setGravity(Gravity.CENTER);
	   title.setTextColor(Color.WHITE);
	   title.setTextSize(20);
	
	  dialogbuilder.setCustomTitle(title);
      
      dialogbuilder.setCancelable(false);
      dialogDetails = dialogbuilder.create();
      ///////////////////////////
      
      Log.i("A", "1");
      spinner1 = (Spinner)dialogview.findViewById(R.id.spinner1);
     
      Log.i("A", "2");
      ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, aa);
      Log.i("A", "3");
      adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
      Log.i("A", "4");
      
      
      spinner1.setAdapter(adapter);
      Log.i("A", "5");
      
      spinner1.setPrompt("지역을 선택하세요");
      
      //spinner1.setSelection(2);
      //spinner.setOnItemSelectedListener(new MyOnItemSelectedListener());      
            
      
      
      
      spinner1.getSelectedItemPosition();
      
      
      
      
      ///////////////////////////
      break;
     }
     return dialogDetails;
    }
    
    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {
    
     switch (id) {
     case DIALOG_LOGIN:
      final AlertDialog alertDialog = (AlertDialog) dialog;
      Button loginbutton = (Button) alertDialog.findViewById(R.id.btn_login);
      g_Name = (EditText) alertDialog.findViewById(R.id.txt_name);
      g_Email = (EditText) alertDialog.findViewById(R.id.txt_email);
      //CheckBox g_checkbox = (CheckBox) findViewById(R.id.checkSave);
      //Log.i("onPrepareDialog", "14"); 
      if ( !strName.isEmpty() )
      {
    	  //Log.i("onPrepareDialog", "13.5"); 
	      g_Name.setText(strName);
	      
	      
	      spinner1.setSelection( Integer.parseInt(strLocal) );
	   // g_PhoneNumber.setText(strPhoneNumber);
	      
	      
	      if ( !strEmail.isEmpty() )
	    	  g_Email.setText(strEmail+"");
	      
	      int position = g_Name.length();
	      Editable etext = g_Name.getText();
	      Selection.setSelection(etext, position);
      }
      loginbutton.setOnClickListener(new View.OnClickListener() {
    
       @Override
       public void onClick(View v) {
    	   if(spinner1.getSelectedItemPosition() == 0){ 			Toast.makeText(IntroActivity.this,"지역을 선택하세요",Toast.LENGTH_SHORT).show();     		  return; }
    	   if(g_Name.getText().toString().isEmpty()){     		  	Toast.makeText(IntroActivity.this,"이름을 입력하세요",Toast.LENGTH_SHORT).show();     		  return; }
    	   if(g_Email.getText().toString().isEmpty()){      		Toast.makeText(IntroActivity.this,"이메일 주소를 입력하세요",Toast.LENGTH_SHORT).show();      		  return; }     	  

    	   int intLocal = spinner1.getSelectedItemPosition();
    	   
    	//   Log.i(aa[intLocal].toString(), g_Email.getText().toString());    	   
    	    SetUserInfo();

    	    sendpost(aa[intLocal].toString(), g_Email.getText().toString());
    	    //sendpost("aaa", "bbb");

    		alertDialog.dismiss();
			Intent i = new Intent(IntroActivity.this, Browser.class);
			startActivity(i);
			finish();
       }
      });
      break;
     }
    }
    
    public void sendpost(String affiliation, String email){
        try {
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }

            // Create a new trust manager that trust all certificates
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }
                        public void checkClientTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }
                        public void checkServerTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };

            // Activate the new trust manager
            try {
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            } catch (Exception e) {}

            ArrayList<NameValuePair> post = new ArrayList<NameValuePair>();
            post.add(new BasicNameValuePair("type", "POL-HASH APP"));
            post.add(new BasicNameValuePair("company", ""));
            post.add(new BasicNameValuePair("affiliation", affiliation));
            post.add(new BasicNameValuePair("contentone", email));
            post.add(new BasicNameValuePair("contenttwo", "1.0")); 

            // 연결 HttpClient 객체 생성
            HttpClient client = new DefaultHttpClient();

            // 객체 연결 설정 부분, 연결 최대시간 등등
            HttpParams params = client.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 1000);
            HttpConnectionParams.setSoTimeout(params, 1000);

            // Post객체 생성
            HttpPost httpPost = new HttpPost("");

            try {
                UrlEncodedFormEntity entity = new UrlEncodedFormEntity(post, "UTF-8");
                httpPost.setEntity(entity);
                client.execute(httpPost); // 전송
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }catch(Exception e){e.printStackTrace(); }
    }
    
    @SuppressWarnings("resource")
	public void GetUserInfo() 
    { 
    	try
    	{
        File file;
        file = new File(this.getFilesDir().getPath());
        if(!file.exists()){
         file.mkdirs();
        }
        file = new File(this.getFilesDir().getPath()+"/polhash.xml");
        if(!file.exists()){
        	strName = "";
        	strLocal = "";
        	strEmail = "";
            return;
           }
        String body = "";
        StringBuffer bodytext = new StringBuffer();
        FileInputStream fis = new FileInputStream(file);
        BufferedReader bufferReader = new BufferedReader(new InputStreamReader(fis,"UTF-8"));
        int linecount = 0;
        while((body = bufferReader.readLine())!=null)
        {
         bodytext.append(body);
         if ( linecount == 0 )
        	 strLocal = body;
         if ( linecount == 1 )
        	 strName = body;
         if ( linecount == 2 )
         {
        	 strEmail = body;
         }
         linecount++;
        }
	
        // Toast.makeText(IntroActivity.this, bodytext.toString(), Toast.LENGTH_SHORT).show();
        
        if ( linecount == 3 )
        {
        	
        } else
        {
        	file.delete();
        }
	    }catch(IOException e){
	        
	    }
    }

    public void SetUserInfo() 
    {
        File file;
        String lstremail = g_Email.getText().toString();
        lstremail = lstremail.replaceAll(", "");
        String body = spinner1.getSelectedItemPosition() + "\n" + g_Name.getText().toString() + "\n" + lstremail;
        file = new File(this.getFilesDir().getPath());
        if(!file.exists()){
         file.mkdirs();
        }
        file = new File(this.getFilesDir().getPath()+"");
        file.delete();
        
        try{
         FileOutputStream fos = new FileOutputStream(file);
         BufferedWriter buw = new BufferedWriter(new OutputStreamWriter(fos, "UTF8"));
         buw.write(body);
         buw.close();
         fos.close();
        }catch(IOException e){ }
    }
}

