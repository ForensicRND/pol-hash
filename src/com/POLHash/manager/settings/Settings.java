/*
 * Copyright (C) 2014 Simple Explorer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

package com.POLHash.manager.settings;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.POLHash.manager.R;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.StatFs;
import android.preference.PreferenceManager;

public final class Settings {

	public static boolean showthumbnail;
	public static boolean mShowHiddenFiles;
	public static int mListAppearance;
	public static int mSortType;
	public static int mTheme;
	public static String defaultdir;
	
	//
	public static String default_Name;
	public static String default_Phonenumber;
	
	
	private static SharedPreferences p;

	public static void updatePreferences(Context context) {
		p = PreferenceManager.getDefaultSharedPreferences(context);

		mShowHiddenFiles = p.getBoolean("displayhiddenfiles", true);
		showthumbnail = p.getBoolean("showpreview", true);
		mTheme = Integer.parseInt(p.getString("preference_theme",
				Integer.toString(R.style.ThemeLight)));
		mSortType = Integer.parseInt(p.getString("sort", "1"));
		mListAppearance = Integer.parseInt(p.getString("viewmode", "1"));
		
		defaultdir = Environment.getExternalStorageDirectory().getPath();
		
		
	}
	public static String getMicroSDCardDirectory() {
	    List<String> mMounts = readMountsFile();
	    List<String> mVold = readVoldFile();
	     
	    for (int i=0; i < mMounts.size(); i++) {
	        String mount = mMounts.get(i);
	         
	        if (!mVold.contains(mount)) {
	            mMounts.remove(i--);
	            continue;
	        }
	         
	        File root = new File(mount);
	        if (!root.exists() || !root.isDirectory()) {
	            mMounts.remove(i--);
	            continue;
	        }
	         
	        if (!isAvailableFileSystem(mount)) {
	            mMounts.remove(i--);
	            continue;
	        }
	         
	        if (!checkMicroSDCard(mount)) {
	            mMounts.remove(i--);
	        }
	    }
	     
	    if (mMounts.size() == 1) {
	        return mMounts.get(0);
	    }
	     
	    return null;
	}
	 
	@SuppressWarnings("resource")
	private static List<String> readMountsFile() {
 
	    List<String> mMounts = new ArrayList<String>();
	 
	    try {
	        Scanner scanner = new Scanner(new File("/proc/mounts"));
	         
	        while (scanner.hasNext()) {
	            String line = scanner.nextLine();
	             
	            if (line.startsWith("/dev/block/vold/")) {
	                String[] lineElements = line.split("[ \t]+");
	                String element = lineElements[1];
	                                     
	                mMounts.add(element);
	            }
	        }
	    } catch (Exception e) {
	        // Auto-generated catch block
	        e.printStackTrace();
	    }
	     
	    return mMounts;
	}
	 
	@SuppressWarnings("resource")
	private static List<String> readVoldFile() {

	    List<String> mVold = new ArrayList<String>();
	     
	    try {
	        Scanner scanner = new Scanner(new File("/system/etc/vold.fstab"));
	         
	        while (scanner.hasNext()) {
	            String line = scanner.nextLine();
	             
	            if (line.startsWith("dev_mount")) {
	                String[] lineElements = line.split("[ \t]+");
	                String element = lineElements[2];
	                 
	                if (element.contains(":")) {
	                    element = element.substring(0, element.indexOf(":"));
	                }
	 
	                mVold.add(element);
	            }
	        }
	    } catch (Exception e) {
	        // Auto-generated catch block
	        e.printStackTrace();
	    }
	     
	    return mVold;
	}
	 
	@SuppressWarnings("deprecation")
	private static boolean checkMicroSDCard(String fileSystemName) {
	    StatFs statFs = new StatFs(fileSystemName);
	     
	    long totalSize = (long)statFs.getBlockSize() * statFs.getBlockCount();
	     
	    if (totalSize < 1073741824) {
	        return false;
	    }
	         
	    return true;
	}
	 
	private static boolean isAvailableFileSystem(String fileSystemName) {
	    final String[]  unAvailableFileSystemList = {"/dev", "/mnt/asec", "/mnt/obb", "/system", "/data", "/cache", "/efs", "/firmware"};   
	     
	    for (String name : unAvailableFileSystemList) {
	        if (fileSystemName.contains(name) == true) {
	            return false;
	        }
	    }
	     
	    if (Environment.getExternalStorageDirectory().getAbsolutePath().equals(fileSystemName) == true) {
	        return false;
	    }
	     
	    return true;
	}	
}
