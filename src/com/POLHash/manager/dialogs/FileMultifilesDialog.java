package com.POLHash.manager.dialogs;
 
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jetbrains.annotations.NotNull;

import com.POLHash.manager.R;
import com.POLHash.manager.utils.SimpleUtils;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;

import org.apache.commons.io.FileUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView; 

@SuppressLint("InflateParams") public final class FileMultifilesDialog extends DialogFragment {
	
	private static Activity activity;
	private static File mFile;
	private PropertiesAdapter mAdapter;
	public static String g_MD5;
	public static String g_SHA1;
	public static String g_size;
	public static String g_createtime;
	public static String g_device;
	public static String g_IMEI;
	public static String telPhoneNo;	
	public static String g_Singlereport;
	public static String g_Singlereport_time;
	public static String Currentfile;
	private static Handler mHandler = new Handler();
	private static ProgressDialog pDlg;
	public static String[] files; // 전달된 파일의 경로
	public static String[] hashResults;
	private static String g_strAllHashCreatedTime;
	
	public static String g_TotalFileSize;
	public static long totalfilesizeint;
	public static String g_finishtime;
	public static String g_forSingleFileReport;
	
	public static int g_resultsCount;
	public static long totalfilesizeCurrent;
	
	public static String g_strName;
	public static String g_strPhoneNumber;
	public static String g_strEmail;
	
	public static String[] global_Filename;
	public static String[] global_Size;
	public static String[] global_MD5;
	public static String[] global_SHA1;
	public static String g_PDFCreationTime;
	
	public static TextView m_filelist;
	public static DialogFragment instantiate(File file, String[] files1) {
		mFile = file;
		files = files1;
		final FileMultifilesDialog dialog = new FileMultifilesDialog();
		return dialog;
	}
	private void SendEmail(String subject, String text, Uri filePath) {
		Intent sendIntent = new Intent(Intent.ACTION_SEND);
		sendIntent.setType("text/html");
		sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		sendIntent.putExtra(Intent.EXTRA_STREAM, filePath);
		
		if ( !g_strEmail.isEmpty() )
		{
			g_strEmail = g_strEmail.replaceAll("", "");
			sendIntent.putExtra(android.content.Intent.EXTRA_EMAIL,new String[] { g_strEmail+"" });
		}
		sendIntent.putExtra(Intent.EXTRA_TEXT, text);
		startActivity(Intent.createChooser(sendIntent, "Email:"));
	}
	@Override
	public Dialog onCreateDialog(Bundle state) {
		activity = getActivity();
		final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		mAdapter = new PropertiesAdapter(activity, mFile);
		
		TelephonyManager telephony =  (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);  
	    telPhoneNo = telephony.getLine1Number();  			  
	    g_IMEI = telephony.getDeviceId();
	    
	    
	    
		builder.setTitle("파일 해시값 생성 정보");

		builder.setNeutralButton(R.string.report,
				new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) 
				{	
					//
					GetUserInfo();
					
					File file =  new File( 
						    Environment.getExternalStoragePublicDirectory( 
						    Environment.DIRECTORY_DOWNLOADS), 
						    g_strAllHashCreatedTime + "_HASH.TXT"); 
					String dirPath = file.getPath();

					Log.v(null,"filename: "+ dirPath);
					Log.v(null,"hashResults.length: "+ hashResults.length);
					String emailbodytext = "";
					File savefile = new File(dirPath);
					try{
						FileOutputStream fos = new FileOutputStream(savefile);
						
						// String wStrTemp = "순번, 파일 이름, 크기, 해시 생성 시각, MD5 HASH, SHA1 HASH, 스마트폰내 파일 마지막 쓰기 시각, 스마트폰내 경로\r\n";
						
						// 마지막 생성 시각, 경로 삭제
						String wStrTemp = "순번, 파일 이름, 크기, 해시 생성 시각, MD5 HASH, SHA1 HASH\r\n";
						emailbodytext += wStrTemp;
						fos.write(wStrTemp.getBytes());
						for (int i = 0; i < g_resultsCount ; i++)
						{
							String tmp = "\r\n";
							wStrTemp = hashResults[i] + "\r\n";
							emailbodytext += wStrTemp;
							fos.write(hashResults[i].getBytes());
							fos.write(tmp.getBytes());
						}
						
						wStrTemp = "\r\n총 파일 개수: " + g_resultsCount + "개" + "\r\n";
						emailbodytext += wStrTemp;
						fos.write(wStrTemp.getBytes());
						wStrTemp = "총 파일 크기: " + g_TotalFileSize + "\r\n";
						emailbodytext += wStrTemp;
						fos.write(wStrTemp.getBytes());
						
						wStrTemp = "총 해시 생성 완료 시각: " + g_finishtime + "\r\n";
						emailbodytext += wStrTemp;
						fos.write(wStrTemp.getBytes());
						
						if ( telPhoneNo != null )
							wStrTemp = "해시 생성 스마트폰 정보: " + telPhoneNo + " (" + Build.MODEL + ", IMEI:" + g_IMEI + ")" + "\r\n\r\n";
						else
							wStrTemp = "해시 생성 스마트폰 정보: " + "번호 없음" + " (" + Build.MODEL + ", IMEI:" + g_IMEI + ")" + "\r\n\r\n";
							
						emailbodytext += wStrTemp;
						fos.write(wStrTemp.getBytes());						
						
						wStrTemp = "\r\n";
						fos.write(wStrTemp.getBytes());
						fos.close();
					} catch(IOException e){}
					

					// PDF 생성 시작
					File pdffile =  new File( 
						    Environment.getExternalStoragePublicDirectory( 
						    Environment.DIRECTORY_DOWNLOADS), 
						    g_strAllHashCreatedTime + "_HASH.pdf"); 
					String pdfdirPath = pdffile.getPath();		
					
					//Log.v(null,"pdfdirPath: "+ pdfdirPath);
					
			        Document document = new Document();
			        document.setPageSize(PageSize.A4);
			        
			        try {
						PdfWriter.getInstance(document, new FileOutputStream(pdfdirPath));
					} catch (FileNotFoundException e2) {
						e2.printStackTrace();
					} catch (DocumentException e2) {
						e2.printStackTrace();
					}
			        document.open();

			        try {
						document.add(createFirstTable());
					} catch (DocumentException e1) {
						e1.printStackTrace();
					}
			
			        document.close();					
	
			        // Page 넣기
			        try {
						SetPDFPages(pdfdirPath, g_PDFCreationTime);
					} catch (SQLException | IOException | DocumentException e1) {
						e1.printStackTrace();
					}

			        
			        File sendfile = new File(pdfdirPath);
			        
					final Uri fileUri = Uri.fromFile(sendfile);
					Log.v(null,"fileUri - new : "+ fileUri.getPath());
					}		
			});		

		final View content = activity.getLayoutInflater().inflate(
				R.layout.dialog_multifiles_container, null);
		this.initView(content);
		builder.setView(content);
		final AlertDialog dialog = builder.create();
		dialog.setOnShowListener(new DialogInterface.OnShowListener() {
			@Override
			public void onShow(final DialogInterface dialog) {
				((AlertDialog) dialog).getButton(
						DialogInterface.BUTTON_POSITIVE).setVisibility(
						View.GONE);
			}
		});
		return dialog;
	}
	public void MailToGMail()
	{
	}
	public void SetPDFPages(String strpath, String strtime) throws IOException, DocumentException, SQLException
	{
		String RESULT = strpath+".pages.pdf";
		
        Document document = new Document();
        PdfCopy copy = new PdfCopy(document, new FileOutputStream(RESULT));
        document.open();
        
        PdfReader reader = new PdfReader(strpath);
        int n = reader.getNumberOfPages();
        PdfImportedPage page;
        PdfCopy.PageStamp stamp;

        BaseFont objBaseFont = null;
    		try {
    			objBaseFont = BaseFont.createFont("", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
    		} catch (DocumentException e2) {
    			e2.printStackTrace();
    		} catch (IOException e2) {
    			e2.printStackTrace();
    		}
        Font font = new Font(objBaseFont, 12, Font.NORMAL, BaseColor.GRAY);         
        
        for (int i = 0; i < n; ) {
            page = copy.getImportedPage(reader, ++i);
            stamp = copy.createPageStamp(page);
            
            String strFooter = "전자정보 상세목록 생성 시각 " + strtime + " (전체 " + g_resultsCount + "개 파일, 총 " + n +"쪽 중 " + i +"쪽)";
            Phrase iPhrase = new Phrase(strFooter,font);
            ColumnText.showTextAligned(
                    stamp.getUnderContent(), Element.ALIGN_CENTER,
                    iPhrase,// new Phrase(String.format("(%d/%d)", i, n)),
                    297.5f, 28, 0);
            
            stamp.alterContents();
            copy.addPage(page);
        }
        document.close();
        reader.close();
        
        File from = new File(strpath+".pages.pdf");
        File to = new File(strpath);
        if(from.exists())
        	from.renameTo(to);
        from.delete();
        
	}
    @SuppressLint("SimpleDateFormat") public static PdfPTable createFirstTable() throws DocumentException {

    	BaseFont objBaseFont = null;
		try {
			objBaseFont = BaseFont.createFont("", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
        Font objFont = new Font(objBaseFont, 18, Font.BOLD); 
    	
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(100);
        
        PdfPCell cell;
        cell = new PdfPCell(new Phrase("전자정보 상세목록", objFont));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setBorderWidthLeft(2f);
        cell.setBorderWidthRight(2f);
        cell.setBorderWidthTop(2f);
      //cell.setBorderWidthBottom(2f);
        cell.setColspan(5);
        cell.setFixedHeight(46f);
        table.addCell(cell);
        
        objFont = new Font(objBaseFont, 10, Font.NORMAL); 

        float[] columnWidths = new float[] {5f, 20f, 35f, 25f, 35f};
        table.setWidths(columnWidths);
        
        for ( int i = 0 ; i < g_resultsCount ; i++ )
        {
        	if ( i+1 == g_resultsCount )	cell = NorCellB(""+(i+1));				
       		else				       		cell = NorCell(""+(i+1));				
        	cell.setBorderWidthLeft(2f);	 			     
            cell.setBorderWidthTop(2f); 	        
            cell.setRowspan(2);//cell.setRowspan(4);
            
            table.addCell(cell);
            
        	cell = NorCell("파   일   명");     
            cell.setBorderWidthTop(2f);             table.addCell(cell);
            
        	cell = HashCell("  "+global_Filename[i]); 
        	cell.setBorderWidthTop(2f);
            cell.setColspan(3);    					cell.setBorderWidthRight(2f);          
            table.addCell(cell);
           
            if ( i+1 == g_resultsCount )               	table.addCell(NorCellB("해시값(SHA1)"));	   
            else						            	table.addCell(NorCell("해시값(SHA1)"));	        
            cell = HashCell("  " + global_SHA1[i] );
            cell.setColspan(3); 			 			cell.setBorderWidthRight(2f);
            if ( i+1 == g_resultsCount )	           	cell.setBorderWidthBottom(2f);
            table.addCell(cell);
        }
        
		long now = System.currentTimeMillis();
		Date date = new Date(now);
		SimpleDateFormat sdfNow2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		g_PDFCreationTime = sdfNow2.format(date);
    
        return table;
    }

    public void GetUserInfo() 
    { 
    	try
    	{
        File file;
        file = new File(activity.getFilesDir().getPath());
        if(!file.exists()){
         file.mkdirs();
        }
        file = new File(activity.getFilesDir().getPath()+"/polhash.xml");
        if(!file.exists()){
            return;
           }
        String body = "";
        StringBuffer bodytext = new StringBuffer();
        FileInputStream fis = new FileInputStream(file);
        @SuppressWarnings("resource")
		BufferedReader bufferReader = new BufferedReader(new InputStreamReader(fis,"UTF-8"));
        int linecount = 0;
        while((body = bufferReader.readLine())!=null)
        {
         bodytext.append(body);
         if ( linecount == 0 )
        	 ;
         if ( linecount == 1 )
        	 g_strName = body;
         if ( linecount == 2 )   	 
        	 g_strEmail = body;
         linecount++;
        }
        if ( linecount == 3 )
        {
        	
        } else
        {
        	file.delete();
        }
	    }catch(IOException e){
	        
	    }
    }
    private static PdfPCell HashCell(String text){
        
        BaseFont objBaseFont = null;
    		try {
    			objBaseFont = BaseFont.createFont("", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
    		} catch (DocumentException e2) {
    			e2.printStackTrace();
    		} catch (IOException e2) {
    			e2.printStackTrace();
    		}
        Font font = new Font(objBaseFont, 12, Font.NORMAL, BaseColor.BLACK); 

        Phrase phra = new Phrase(text,font);
        PdfPCell cell = new PdfPCell(phra);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setFixedHeight(29f);
        return cell;
    } 
    
    private static PdfPCell NorCellB(String text){
        
        BaseFont objBaseFont = null;
    		try {
    			objBaseFont = BaseFont.createFont("", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
    		} catch (DocumentException e2) {
    			e2.printStackTrace();
    		} catch (IOException e2) {
    			e2.printStackTrace();
    		}
        Font font = new Font(objBaseFont, 12, Font.NORMAL, BaseColor.BLACK); 

        Phrase phra = new Phrase(text,font);
        PdfPCell cell = new PdfPCell(phra);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setFixedHeight(29f);
        cell.setBorderWidthBottom(2f);
        return cell;
    }
    
    private static PdfPCell NorCell(String text){
       
        BaseFont objBaseFont = null;
    		try {
    			objBaseFont = BaseFont.createFont("", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
    		} catch (DocumentException e2) {
    			e2.printStackTrace();
    		} catch (IOException e2) {
    			e2.printStackTrace();
    		}
        Font font = new Font(objBaseFont, 12, Font.NORMAL, BaseColor.BLACK); 

        Phrase phra = new Phrase(text,font);
        PdfPCell cell = new PdfPCell(phra);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setFixedHeight(29f);
        return cell;
    }
    private static PdfPCell BottomCell(String text){
        
        BaseFont objBaseFont = null;
    		try {
    			objBaseFont = BaseFont.createFont("", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
    		} catch (DocumentException e2) {
    			e2.printStackTrace();
    		} catch (IOException e2) {
    			e2.printStackTrace();
    		}
        Font font = new Font(objBaseFont, 12, Font.NORMAL, BaseColor.BLACK); 
        Phrase phra = new Phrase(text,font);
        PdfPCell cell = new PdfPCell(phra);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        return cell;
    }
    private void initView(@NotNull final View view) {
		final ViewPager pager = (ViewPager) view
				.findViewById(R.id.tabsContainer);
		pager.setAdapter(mAdapter);

		final CompoundButton tab1 = (CompoundButton) view
				.findViewById(R.id.tab1);
		final CompoundButton tab2 = (CompoundButton) view
				.findViewById(R.id.tab2);

		pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				tab1.setChecked(position == 0);
				tab2.setChecked(position == 1);
				((AlertDialog) getDialog())
						.getButton(DialogInterface.BUTTON_POSITIVE)
						.setVisibility(
								position == 0
										|| !((FilePermissionsPagerItem) mAdapter
												.getItem(1)).areBoxesEnabled() ? View.GONE
										: View.VISIBLE);
			}
		});

		tab1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tab1.setChecked(true);
				tab2.setChecked(false);
				pager.setCurrentItem(0);
			}
		});

		tab2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tab2.setChecked(true);
				tab1.setChecked(false);
				pager.setCurrentItem(1);
			}
		});
	}
	@Override
	public void onStart() {
		super.onStart();
		mAdapter.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();
		mAdapter.onStop();
	}

	private interface PagerItem {

		void onStart();
		void onStop();

		@NotNull
		View onCreateView(@NotNull LayoutInflater inflater);
	}

	private static final class PropertiesAdapter extends PagerAdapter {

		private final LayoutInflater mLayoutInflater;
		private final File mFile;
		private final PagerItem[] mItems;

		private PropertiesAdapter(@NotNull final Activity context,
				@NotNull final File file) {
			mLayoutInflater = context.getLayoutInflater();
			mFile = file;
			mItems = new PagerItem[] { new FilePropertiesPagerItem(mFile),
					new FilePermissionsPagerItem(mFile) };
		}

		void onStart() {
			for (final PagerItem item : mItems) {
				item.onStart();
			}
		}

		void onStop() {
			for (final PagerItem item : mItems) {
				item.onStop();
			}
		}

		@NotNull
		PagerItem getItem(final int position) {
			return mItems[position];
		}

		@Override
		public Object instantiateItem(final ViewGroup container,
				final int position) {
			final PagerItem item = mItems[position];
			final View view = item.onCreateView(mLayoutInflater);
			container.addView(view);
			item.onStart();
			return view;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object); 
		}

		@Override
		public int getCount() {
			return mItems.length;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}
	}

	private static final class FilePropertiesPagerItem implements PagerItem {
		private File file3;
		private TextView mFileCount, mTimeLabel, mSizeLabel, mDeviceLabel;
		private View mView;
		private LoadFsTask mTask;
		private FilePropertiesPagerItem(File file) {
			this.file3 = file;
		}

		@NotNull
		@Override
		public View onCreateView(@NotNull final LayoutInflater inflater) {
			mView = inflater.inflate(R.layout.dialog_multifiles, null);
			initView(mView);
			return mView;
		}

		@Override
		public void onStart() {
			if (mView != null) {
				if (mTask == null) {
					mTask = new LoadFsTask();
					mTask.execute(mFile);
				}
			}
		}

		@Override
		public void onStop() {
			if (mTask != null && mTask.getStatus() == AsyncTask.Status.RUNNING) {
				mTask.cancel(true);
			}
		}

		private void initView(View table) {
			this.mFileCount = (TextView) mView.findViewById(R.id.filecount);
			//this.mPathLabel = (TextView) mView.findViewById(R.id.path_label);
			this.mTimeLabel = (TextView) mView.findViewById(R.id.time_stamp2);
			this.mSizeLabel = (TextView) mView.findViewById(R.id.total_size);
			//this.mMD5Label = (TextView) mView.findViewById(R.id.md5_summary);
			//this.mSHA1Label = (TextView) mView.findViewById(R.id.sha1_summary);
			this.mDeviceLabel = (TextView) mView.findViewById(R.id.device_summary);		
		}
		private final class LoadFsTask extends AsyncTask<File, Void, String> {
			private String mDisplaySize;
			private long size = 0;

			@Override
			protected void onPreExecute() {
				mFileCount.setText("계산 중...");	
				mTimeLabel.setText("계산 중...");
				mSizeLabel.setText("계산 중...");
				mDeviceLabel.setText(Build.MODEL + " (IMEI:" + g_IMEI + ")");
			}

			@Override
			protected String doInBackground(final File... params) {

				if (!file3.canRead()) {
					mDisplaySize = "---";
					return mDisplaySize;
				} else if (file3.isFile()) {
					size = file3.length();
				} else {
					size = SimpleUtils.getDirSize(file3);
				}
				
				mDisplaySize = FileUtils.byteCountToDisplaySize(size);
				DecimalFormat format = new DecimalFormat("###,###");
				String tmp = format.format(size);				

				g_size = tmp + " 바이트 (" + mDisplaySize +")";
				return tmp + " 바이트 (" + mDisplaySize +")";
			}
			
			public void setResutText() {
				mHandler.post(new Runnable() {
							
							@Override
							public void run() {
								//mMD5Label.setText(g_MD5);
								//mSHA1Label.setText(g_SHA1);	
								mFileCount.setText("총 " + g_resultsCount + "개");	
								mSizeLabel.setText(g_TotalFileSize);
								mTimeLabel.setText(g_finishtime);
								
								if ( files.length != 0 )
									m_filelist.setText(g_forSingleFileReport);
								
							}
						});
			}

			@Override
			protected void onPostExecute(final String result) {
			
			try {			
			
					global_Filename = new String[files.length];
					global_Size = new String[files.length];
					global_MD5 = new String[files.length];
					global_SHA1 = new String[files.length];
				
					pDlg = new ProgressDialog(activity);
					pDlg.setMessage("파일 해시값 계산 중 입니다...\n개수가 많으면 시간이 소요 됩니다...");
					pDlg.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
					pDlg.setCancelable(false);
					
					pDlg.setProgressNumberFormat(null);
					
					totalfilesizeCurrent = 0;
					totalfilesizeint = 0;
					
					for ( int i = 0 ; i < files.length ; i++ ){
						File file = new File(files[i]);	
						if ( file.canRead() && !file.isDirectory() ){	
							long size = file.length();
							totalfilesizeint += size;	
						}
					}		
					totalfilesizeCurrent = totalfilesizeint;
					pDlg.setProgress(0);
					pDlg.setMax(10000);
					pDlg.show();	
					
					new Thread(new Runnable() {
						
						@SuppressLint({ "SimpleDateFormat", "DefaultLocale" }) @Override
						public void run() {
							hashResults = new String[files.length];
							try {	
								
								g_resultsCount = 0;
								g_forSingleFileReport = "";
								for ( int i = 0 ; i < files.length ; i++ )
								{
									File file = new File(files[i]);
									g_MD5 = "";
									g_SHA1 = "";															
									if ( file.canRead() && !file.isDirectory() )
									{	
										Log.v(null,"i: "+ i);
									
										//
										global_Filename[g_resultsCount] = file.getName();
										
										long size = file.length();
										Log.v(null,"size: "+ size);
										String mDisplaySize = FileUtils.byteCountToDisplaySize(size);
										DecimalFormat format = new DecimalFormat("###,###");
										String tmp = format.format(size);				
										String strfilesize = tmp + " 바이트 (약 " + mDisplaySize +")";
										
										//
										global_Size[g_resultsCount] = strfilesize;
										
										
										if ( !file.canRead() || file.isDirectory() ){										
											g_MD5 = "FILE ERROR";
											g_SHA1 = "FILE ERROR";
										} else	{
											
											// 마지막 쓰기 시간 // strlastmodifiedtime			
											SimpleDateFormat sdffile = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");			
											sdffile.format(file.lastModified());
											
											// MD5 생성 시작
											InputStream fis = new FileInputStream(files[i]);
											byte[] buffer = new byte[8192];
											MessageDigest complete = MessageDigest.getInstance("MD5");
											int numRead;
											do {
												numRead = fis.read(buffer);
												if (numRead > 0) {
													complete.update(buffer, 0, numRead);
													totalfilesizeCurrent = totalfilesizeCurrent-(numRead/2);										
													int CProgr1 = (int) ((totalfilesizeCurrent/(double)totalfilesizeint)*10000);
													setProgressVal(10000-CProgr1);	
													}
											} while (numRead != -1);
											fis.close();
											g_MD5 = GetHexString(complete.digest()).toUpperCase();	
											global_MD5[g_resultsCount] = g_MD5;
											// MD5 생성 끝
																						
											// SHA1 생성 시작
											InputStream fis2 = new FileInputStream(files[i]);
											byte[] buffer2 = new byte[8192];
											MessageDigest complete2 = MessageDigest.getInstance("SHA1");
											int numRead2;
											do {
												numRead2 = fis2.read(buffer2);
												if (numRead2 > 0) {
													complete2.update(buffer2, 0, numRead2);
													totalfilesizeCurrent = totalfilesizeCurrent-(numRead2/2);
													int CProgr2 = (int) ((totalfilesizeCurrent/(double)totalfilesizeint)*10000);
													setProgressVal(10000-CProgr2);								
												}
											} while (numRead2 != -1);
											fis2.close();
											g_SHA1 = GetHexString(complete2.digest()).toUpperCase();	
											global_SHA1[g_resultsCount] = g_SHA1;
											// SHA1 생성 끝

											long now = System.currentTimeMillis();
											Date date = new Date(now);
											SimpleDateFormat sdfNow2 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
											String nowtime1 = sdfNow2.format(date);

											String resultsStr = (g_resultsCount+1) + ", " + file.getName() + ", "+ strfilesize +", "+ nowtime1 +", "+ g_MD5 +", "+ g_SHA1 ;	
											hashResults[g_resultsCount++] =  resultsStr;
											
											if ( files.length == 1 )
											{
												g_forSingleFileReport = "대상 파일 이름:\n" + file.getName() + "\r\n\n" +
														"파일 크기:\n" + strfilesize + "\r\n\n" +
														"해시 생성 시각:\n" + nowtime1 + "\r\n\n" +
														"MD5 해시:\n" + g_MD5 + "\r\n\n" +
														"SHA1 해시:\n" + g_SHA1;
											}
											
											else if ( files.length > 1 )
											{
												g_forSingleFileReport  += (i+1) + ": 대상 파일 이름:\n" + file.getName() + "\r\n\n" +
														"파일 크기:\n" + strfilesize + "\r\n\n" +
														"해시 생성 시각:\n" + nowtime1 + "\r\n\n" +
														"MD5 해시:\n" + g_MD5 + "\r\n\n" +
														"SHA1 해시:\n" + g_SHA1 + "\r\n\n";
											}

											
										}
									} else
									{
										g_MD5 = "FILE ERROR";
										g_SHA1 = "FILE ERROR";									
									}
								}
								
								long now = System.currentTimeMillis();
								Date date = new Date(now);
								SimpleDateFormat sdfNow = new SimpleDateFormat("yyyyMMddHHmmss");
								SimpleDateFormat sdfNow2 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
								g_strAllHashCreatedTime = 	sdfNow.format(date);
								g_finishtime = sdfNow2.format(date);
								
								// 크기 측정
								String mDisplaySize = FileUtils.byteCountToDisplaySize(totalfilesizeint);
								DecimalFormat format = new DecimalFormat("###,###");
								String tmp = format.format(totalfilesizeint);	
								g_TotalFileSize = tmp + " 바이트 (" + mDisplaySize +")";
								Log.v(null,"tmp: "+ tmp);
								Log.v(null,"mDisplaySize: "+ mDisplaySize);
								pDlg.cancel();
								setResutText();
								
							} catch (Exception e) {
								e.printStackTrace();
							}
													
						}
					}).start();
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}
	
	public static String GetHexString(byte[] b) throws Exception {
		String result = "";
			for (int i = 0; i < b.length; i++)
				result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
		return result;
	}

	public static void setProgressVal(final int val) {
		mHandler.post(new Runnable() {
			@Override
			public void run() {
				pDlg.setProgress(val);
			}
		});
	}
	
	private static final class FilePermissionsPagerItem implements PagerItem,
			CompoundButton.OnCheckedChangeListener {

		private final File mFile;
		private View mView;
		private LoadFsTask mTask;
		private FilePermissionsPagerItem(final File file) {
			mFile = file;
		}

		@NotNull
		@Override 
		public View onCreateView(@NotNull final LayoutInflater inflater) {
			mView = inflater.inflate(R.layout.dialog_multifiles_details, null);
			initView(mView);
			return mView;
		}

		@Override
		public void onStart() {
			if (mView != null) {
				if (mTask == null) {
					mTask = new LoadFsTask(this);
				}
				if (mTask.getStatus() != AsyncTask.Status.RUNNING) {
					mTask.execute(mFile);
				}
			}
		}

		@Override
		public void onStop() {
			if (mTask != null && mTask.getStatus() == AsyncTask.Status.RUNNING) {
				mTask.cancel(false);
			}
		}
			
		private void initView(View table) {

			m_filelist = (TextView) mView.findViewById(R.id.fileslist);
			String lDisplaySize;
			long lsize = 0;
			String fileliststr = "";
			long readfilesCount = 0;
			for (int i = 0 ; i < files.length ; i++ )
			{
				File file = new File(files[i]);
				String[] directoryName = files[i].split("/");  
				String sfileName = directoryName[directoryName.length -1];
				
				if (!file.canRead()) {
					lDisplaySize = "---";
				} else if (file.isFile()) {
					lsize = file.length();
				} else {
					lsize = SimpleUtils.getDirSize(file);
				}
				
				lDisplaySize = FileUtils.byteCountToDisplaySize(lsize);
				DecimalFormat format = new DecimalFormat("###,###");
				format.format(lsize);	
				
				String strSize = "";
				if ( file.isFile() )
				{
					strSize = " (" + lDisplaySize +")";
					fileliststr += readfilesCount+1 +", " + sfileName + strSize + "\n";
					readfilesCount++;
				}
				else
				{
					strSize = "(폴더)";
				}
				
			}
			m_filelist.setText(fileliststr);
		}

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
		}

		private class LoadFsTask extends AsyncTask<File, Void, String> {

			private final WeakReference<FilePermissionsPagerItem> mItemRef;

			private LoadFsTask(@NotNull final FilePermissionsPagerItem item) {
				this.mItemRef = new WeakReference<FilePermissionsPagerItem>(
						item);
			}

			@Override
			protected String doInBackground(final File... params) {
				return params[0].getAbsolutePath();
			}

			@Override
			protected void onPostExecute(final String file) {
				final FilePermissionsPagerItem item = mItemRef.get();
				if (item != null) {
					if (file == null) {
						item.disableBoxes();
					}
				}
			}
		}

		boolean areBoxesEnabled() {
			return false;
		}

		private void disableBoxes() {

		}
	}
	
}
 